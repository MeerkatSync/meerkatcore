//
//  TokenResponseBody.swift
//  
//
//  Created by Filip Klembara on 24/04/2020.
//

public struct TokenResponseBody: Codable {
    public let token: String
    public init(token: String) {
        self.token = token
    }
}
