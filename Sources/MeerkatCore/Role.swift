//
//  Role.swift
//  
//
//  Created by Filip Klembara on 16/01/2020.
//

public enum Role: String, Codable {
    case owner
    case readAndWrite
    case read
}
