//
//  Update.swift
//  
//
//  Created by Filip Klembara on 16/01/2020.
//

import Foundation

public protocol Update {
    var attribute: String { get }
    var value: UpdateValue { get }
}

public enum UpdateValue {
    public struct ArrayDiff: Codable {
        public let original: [String]
        public let difference: CollectionDifference<String>
        public init(original: [String], difference: CollectionDifference<String>) {
            self.original = original
            self.difference = difference
        }

        public init(original: [String], new: [String]) {
            self.original = original
            self.difference = new.difference(from: original)
        }
    }
    case string(String)
    case int(Int)
    case data(Data)
    case bool(Bool)
    case uint(UInt)
    case double(Double)
    case `nil`
    case date(Date)
    case object(id: String)
    case array([String])
    case arrayDiff(ArrayDiff)
}
