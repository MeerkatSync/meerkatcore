//
//  CodableGroup.swift
//  
//
//  Created by Filip Klembara on 29/02/2020.
//

public struct CodableGroup: Codable {
    public let id: GroupID
    public let version: UInt64

    public init(id: String, version: UInt64) {
        self.id = id
        self.version = version
    }
}

