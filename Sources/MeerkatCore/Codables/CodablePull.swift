//
//  CodablePull.swift
//  
//
//  Created by Filip Klembara on 01/03/2020.
//

public struct CodablePull: Codable {
    public let schemeVersion: Int
    public let groups: [CodableGroup]

    public init(schemeVersion: Int, groups: [CodableGroup]) {
        self.schemeVersion = schemeVersion
        self.groups = groups
    }
}
