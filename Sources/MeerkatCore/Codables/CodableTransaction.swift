//
//  CodableTransaction.swift
//  
//
//  Created by Filip Klembara on 29/02/2020.
//

public struct CodableTransaction: Codable {
    public let deletions: [CodableSubtransaction]

    public let creations: [CodableCreationDescription]

    public let modifications: [CodableSubtransaction]

    public init(deletions: [CodableSubtransaction], creations: [CodableCreationDescription], modifications: [CodableSubtransaction]) {
        self.deletions = deletions
        self.creations = creations
        self.modifications = modifications
    }
}
