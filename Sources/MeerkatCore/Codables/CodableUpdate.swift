//
//  CodableUpdate.swift
//  
//
//  Created by Filip Klembara on 29/02/2020.
//

import Foundation

public struct CodableUpdate: Codable  {
    public let attribute: String
    public let value: Data?

    public init(attribute: String, value: Data?) {
        self.attribute = attribute
        self.value = value
    }
}
