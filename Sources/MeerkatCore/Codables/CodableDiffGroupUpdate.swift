//
//  CodableDiffGroupUpdate.swift
//  
//
//  Created by Filip Klembara on 29/02/2020.
//

public struct CodableDiffGroupUpdate: Codable {
    public let groupId: String
    public let version: GroupVersion
    public let role: Role

    public init(groupId: String, version: GroupVersion, role: Role) {
        self.groupId = groupId
        self.version = version
        self.role = role
    }
}
