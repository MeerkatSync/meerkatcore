//
//  LogCodable.swift
//  
//
//  Created by Filip Klembara on 19/02/2020.
//

public struct CodableDiff: Codable {
    public let transactions: [CodableTransaction]
    public let groups: [CodableDiffGroupUpdate]

    public init(transactions: [CodableTransaction], groups: [CodableDiffGroupUpdate]) {
        self.transactions = transactions
        self.groups = groups
    }
}
