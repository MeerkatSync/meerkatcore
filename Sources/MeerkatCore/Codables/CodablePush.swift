//
//  CodablePush.swift
//  
//
//  Created by Filip Klembara on 20/02/2020.
//

import Foundation

public struct CodablePush: Codable {
    public let schemeVersion: Int
    public let groups: [CodableGroup]
    public let transaction: CodableTransaction

    public init(schemeVersion: Int, groups: [CodableGroup], transaction: CodableTransaction) {
        self.schemeVersion = schemeVersion
        self.groups = groups
        self.transaction = transaction
    }
}
