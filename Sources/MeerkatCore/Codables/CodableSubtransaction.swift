//
//  CodableSubtransaction.swift
//  
//
//  Created by Filip Klembara on 29/02/2020.
//

import Foundation

public struct CodableSubtransaction: Codable {
    public let timestamp: Date
    public let updates: [CodableUpdate]
    public let className: String
    public let objectId: ObjectID

    public init(timestamp: Date, updates: [CodableUpdate], className: String, objectId: ObjectID) {
        self.timestamp = timestamp
        self.updates = updates
        self.className = className
        self.objectId = objectId
    }
}
