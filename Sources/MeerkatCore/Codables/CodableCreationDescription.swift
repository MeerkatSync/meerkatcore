//
//  CodableCreationDescription.swift
//  
//
//  Created by Filip Klembara on 29/02/2020.
//

public struct CodableCreationDescription: Codable {
    public let groupID: GroupID
    public let object: CodableSubtransaction

    public init(groupID: GroupID, object: CodableSubtransaction) {
        self.groupID = groupID
        self.object = object
    }
}
