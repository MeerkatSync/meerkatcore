//
//  SubTransaction.swift
//  
//
//  Created by Filip Klembara on 16/01/2020.
//

import Foundation

public protocol SubTransaction: ObjectDescription {
    var timestamp: Date { get }
    var updates: [Update] { get }
}
