//
//  Diff.swift
//  
//
//  Created by Filip Klembara on 16/01/2020.
//

public protocol Diff {
    var groups: [DiffGroupUpdate] { get }
    var transactions: [Transaction] { get }
}

public protocol DiffGroupUpdate: InfoElement {
    var newObjects: [ObjectDescription] { get }
}
