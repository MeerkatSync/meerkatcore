//
//  Transaction.swift
//  
//
//  Created by Filip Klembara on 16/01/2020.
//

public protocol Transaction {
    var deletions: [SubTransaction] { get }
    var creations: [CreationDescription] { get }
    var modifications: [SubTransaction] { get }
}

public protocol CreationDescription {
    var groupID: GroupID { get }
    var object: SubTransaction { get }
}
