//
//  Info.swift
//  MeerkatCore
//
//  Created by Filip Klembara on 04/02/2020.
//

import Foundation

public typealias Info = Array<InfoElement>

public protocol InfoElement {
    var group: Group { get }
    var role: Role { get }
}
