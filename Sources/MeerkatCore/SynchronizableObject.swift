//
//  SynchronizableObject.swift
//  
//
//  Created by Filip Klembara on 16/01/2020.
//

import Foundation

public typealias ObjectID = String

public protocol SynchronizableObject {
    var id: ObjectID { get }
}
