//
//  Group.swift
//  
//
//  Created by Filip Klembara on 16/01/2020.
//

public typealias GroupID = String
public typealias GroupVersion = UInt64

public protocol Group {
    var id: GroupID { get }
    var version: GroupVersion { get }
}
