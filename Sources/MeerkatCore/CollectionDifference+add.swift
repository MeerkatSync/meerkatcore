//
//  CollectionDifference+add.swift
//  
//
//  Created by Filip Klembara on 26/02/2020.
//

extension CollectionDifference.Change {
    var offset: Int {
        switch self {
        case let .insert(off, _, _), let .remove(off, _, _):
            return off
        }
    }
    var element: ChangeElement {
        switch self {
        case let .insert(_, element, _), let .remove(_, element, _):
            return element
        }
    }

    var isInsertion: Bool {
        guard case .insert = self else {
            return false
        }
        return true
    }

    func addToIndex(_ v: Int) -> Self {
        switch self {
        case .insert(let offset, let element, _):
            return .insert(offset: offset + v, element: element, associatedWith: nil)
        case .remove(let offset, let element, _):
            return .remove(offset: offset + v, element: element, associatedWith: nil)
        }
    }
}

extension CollectionDifference where ChangeElement: Hashable {
    enum LeftRight {
        case left(CollectionDifference<ChangeElement>.Change)
        case right(CollectionDifference<ChangeElement>.Change)

        var isLeft: Bool {
            guard case .left = self else {
                return false
            }
            return true
        }

        var offset: Int {
            switch self {
            case let .left(x), let .right(x):
                return x.offset
            }
        }
    }

    public func combine(with b: CollectionDifference<ChangeElement>) -> CollectionDifference<ChangeElement> {
        let a = self

        let aDelsIndexes = Set(a.removals.map { $0.offset })
        let bDelsIndexes = Set(b.removals.map { $0.offset })

        let aDels = a.removals.filter { !bDelsIndexes.contains($0.offset) }
        let bDels = b.removals.filter { !aDelsIndexes.contains($0.offset) }

        func prep(lInsertions: [CollectionDifference<ChangeElement>.Change], rDeletions: [CollectionDifference<ChangeElement>.Change]) -> [CollectionDifference<ChangeElement>.Change] {
            lInsertions.map { c -> CollectionDifference<ChangeElement>.Change in
                let lowerDels = rDeletions.filter { $0.offset < c.offset }.count
                return c.addToIndex(-lowerDels)
            }
        }

        let prepA = prep(lInsertions: a.insertions, rDeletions: bDels)
        let prepB = prep(lInsertions: b.insertions, rDeletions: aDels)
        let resDels = a.removals + bDels
        let insA = prepA.map { LeftRight.left($0) }
        let insB = prepB.map { LeftRight.right($0) }
        var leftOff = 0
        var righOff = 0
        let allIns = (insA + insB).sorted { a, b in
            if a.offset == b.offset {
                return a.isLeft
            }
            return a.offset < b.offset
        }
        let ofsetted = allIns.map { x -> CollectionDifference<ChangeElement>.Change in
            switch x {
            case .left(let l):
                leftOff += 1
                return l.addToIndex(righOff)
            case .right(let r):
                righOff += 1
                return r.addToIndex(leftOff)
            }
        }
        let d = CollectionDifference<ChangeElement>(ofsetted + resDels)!
        return d
    }

    public static func += (_ lhs: inout Self, _ rhs: Self) {
        lhs = lhs.combine(with: rhs)
    }

    public static func +(_ lhs: Self, _ rhs: Self) -> Self {
        lhs.combine(with: rhs)
    }
}
