//
//  SyncIDResponseBody.swift
//  
//
//  Created by Filip Klembara on 24/04/2020.
//

public struct SyncIDResponseBody: Codable {
    public let syncId: String
    public init(syncId: String) {
        self.syncId = syncId
    }
}
