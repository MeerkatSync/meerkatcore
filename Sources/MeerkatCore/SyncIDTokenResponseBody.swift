//
//  SyncIDTokenResponseBody.swift
//  
//
//  Created by Filip Klembara on 24/04/2020.
//

public struct SyncIDTokenResponseBody: Codable {
    public let syncId: String
    public let token: String

    public init(syncId: String, token: String) {
        self.syncId = syncId
        self.token = token
    }
}
