//
//  ObjectDescription.swift
//  
//
//  Created by Filip Klembara on 16/01/2020.
//

public protocol ObjectDescription {
    var className: String { get }
    var objectId: ObjectID { get }
}
