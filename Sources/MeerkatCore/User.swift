//
//  User.swift
//  
//
//  Created by Filip Klembara on 16/01/2020.
//

public typealias UserID = String

public protocol User {
    var id: UserID { get }
}
