import XCTest
@testable import MeerkatCore

final class MeerkatCoreTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual("\(UpdateValue.nil)", "\(UpdateValue.nil)")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
