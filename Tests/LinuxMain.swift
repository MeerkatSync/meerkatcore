import XCTest

import MeerkatCoreTests

var tests = [XCTestCaseEntry]()
tests += MeerkatCoreTests.allTests()
XCTMain(tests)
