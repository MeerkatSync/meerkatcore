// swift-tools-version:5.1

import PackageDescription

let package = Package(
    name: "MeerkatCore",
    platforms: [
        .iOS("13.0"),
        .macOS("10.15")
    ],
    products: [
        .library(
            name: "MeerkatCore",
            targets: ["MeerkatCore"]),
    ],
    targets: [
        .target(
            name: "MeerkatCore",
            dependencies: []),
        .testTarget(
            name: "MeerkatCoreTests",
            dependencies: ["MeerkatCore"]),
    ]
)
